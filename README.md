# MakerspaceRP

MakerspaceRP is a django-driven Resource Planning - Software allowing user-friendly front desk management for MakerSpaces, Fablabs and 
open workshops.

The software includes capabilities for:
* user-management
* usage-scheduling
* billing
* basic warehouse-management