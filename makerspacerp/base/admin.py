from django.contrib import admin

from .models import WerkstattUser, Transaction, IntroductionContent, IntroductionParticipation, TimeSlot, Category, \
    Consumable

admin.site.register(WerkstattUser)
admin.site.register(Transaction)
admin.site.register(IntroductionContent)
admin.site.register(IntroductionParticipation)
admin.site.register(TimeSlot)
admin.site.register(Category)
admin.site.register(Consumable)