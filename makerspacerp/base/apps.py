from django.apps import AppConfig


class MakerSpaceRPBaseConfig(AppConfig):
    name = 'makerspacerp.base'

    def ready(self):
        import makerspacerp.base.signals