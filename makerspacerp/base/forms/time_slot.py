from django import forms
from makerspacerp.base.forms.widgets import SplitDateTimeWidget
from makerspacerp.base.models import TimeSlot


class TimeSlotForm(forms.ModelForm):
    begin_time = forms.SplitDateTimeField(label='Beginn', widget=SplitDateTimeWidget(date_attrs={'type': 'date',
                                                                                                 'class': 'mb-1'},
                                                                                     time_attrs={'type': 'time'}))
    end_time = forms.SplitDateTimeField(label='Ende', widget=SplitDateTimeWidget(date_attrs={'type': 'date',
                                                                                             'class': 'mb-1'},
                                        time_attrs={'type': 'time'}), required=False)

    class Meta:
        model = TimeSlot
        fields = '__all__'
