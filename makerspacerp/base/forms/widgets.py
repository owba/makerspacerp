from django.forms import SplitDateTimeWidget


class SplitDateTimeWidget(SplitDateTimeWidget):
    template_name = 'owba_dashboard/widgets/splitdatetime.html'
