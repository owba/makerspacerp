from django.db import models

from makerspacerp.base.models import WerkstattUser


class Account(models.Model):
    user = models.OneToOneField(WerkstattUser, on_delete=models.CASCADE)
    balance = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)
    locked = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Konto'
        verbose_name_plural = 'Konten'

    def __str__(self):
        return "{}'s Konto".format(self.user)

    def change_balance(self, amount):
        self.balance = self.balance - amount
        self.save()

        return True

    def lock(self):
        self.locked = True
        self.save()

        return True

    def unlock(self):
        self.locked = False
        self.save()

        return True
