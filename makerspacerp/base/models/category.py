from django.db import models


class Category(models.Model):
    name = models.CharField(verbose_name='Name', max_length=280)

    class Meta:
        verbose_name = 'Kategorie'
        verbose_name_plural = 'Kategorien'

    def __str__(self):
        return self.name
