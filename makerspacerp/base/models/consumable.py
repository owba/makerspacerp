from django.db import models


class Consumable(models.Model):
    class Units(models.TextChoices):
        PIECES = 'pcs', 'Stück'
        METRE = 'm', 'm'
        CENTIMETRE = 'cm', 'cm'
        MILLIMETRE = 'mm', 'mm'
        SQUARE_METRE = 'm²', 'm²'
        CUBIC_METRE = 'm³', 'm³'
        GRAMM = 'g', 'g'
        KILOGRAMM = 'kg', 'kg'
    name_short = models.CharField(verbose_name='Kurzbeschreibung', max_length=280)
    name_exact = models.CharField(verbose_name='Exakte Bezeichnung', max_length=560)
    thumbnail = models.ImageField(verbose_name='Bild', upload_to='consumables', blank=True)
    unit = models.CharField(verbose_name='Einheit', max_length=280, choices=Units.choices, default=Units.PIECES)
    amount = models.DecimalField(verbose_name='Bestand', max_digits=6, decimal_places=2, default=0.00, editable=False)
    dimensions_x = models.DecimalField(verbose_name='Breite', max_digits=15, decimal_places=2, null=True, blank=True)
    dimensions_y = models.DecimalField(verbose_name='Höhe', max_digits=15, decimal_places=2, null=True, blank=True)
    categories = models.ManyToManyField('Category', verbose_name='Kategorien', blank=True)

    class Meta:
        verbose_name = 'Verbrauchsmaterial'
        verbose_name_plural = 'Verbrauchsmaterial'

    def __str__(self):
        return self.name_short
