from django.db import models


class ConsumptionRefill(models.Model):
    class Types(models.TextChoices):
        CONSUMPTION = 'consumption', 'Verbrauch'
        REFILL = 'refill', 'Aufstockung'
    type = models.CharField(verbose_name='Art', max_length=280, choices=Types.choices)
    consumable = models.ForeignKey('Consumable', verbose_name='Verbrauchsmaterial', on_delete=models.CASCADE)
    user = models.ForeignKey('WerkstattUser', verbose_name='Nutzer:in', null=True, blank=True, on_delete=models.SET_NULL)
    amount = models.DecimalField(verbose_name='Menge', max_digits=12, decimal_places=2)

    def __str__(self):
        return "{}: {}".format(self.type, self.consumable)
