from django.db import models


class DashboardSettings(models.Model):
    workshop_basic_interval = models.IntegerField(verbose_name='Werkstatt-BASIC-Abrechnungs-Intervall', default=60)
    workshop_basic_price = models.DecimalField(verbose_name='Werkstatt-BASIC-Intervall-Preis', max_digits=10,
                                               decimal_places=2,  default=2.00)
    workshop_plus_interval = models.IntegerField(verbose_name='Werkstatt-PLUS-Abrechnungs-Intervall', default=60)
    workshop_plus_price = models.DecimalField(verbose_name='Werkstatt-PLUS-Intervall-Preis', max_digits=10,
                                               decimal_places=2, default=3.00)
    machine_interval = models.IntegerField(verbose_name='Maschinen-Abrechnungs-Intervall', default=5)
    machine_price = models.DecimalField(verbose_name='Maschinen-Intervall-Preis', max_digits=10, decimal_places=2, default=0.50)
    max_people = models.IntegerField(verbose_name='Maximale Personenzahl in der Werkstatt', default=5)
    max_reservation_time = models.IntegerField(verbose_name='Maximale Reservierungszeit', default=120)

    def __str__(self):
        return 'Werkstatteinstellungen'

    def save(self, *args, **kwargs):
        self.pk = 1
        super(DashboardSettings, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass

    @classmethod
    def load(cls):
        settings, created = cls.objects.get_or_create(pk=1)
        return settings
