from django.db import models
from makerspacerp.base.models import Tool


class Introduction(models.Model):
    title = models.CharField(verbose_name='Titel', max_length=280)
    tool = models.ForeignKey(Tool, verbose_name='Für Maschine/Werkzeug', on_delete=models.CASCADE, blank=True, null=True)
    mandatory = models.BooleanField(verbose_name='Braucht jeder?', default=False)
    description = models.TextField(verbose_name='Beschreibung', blank=True)

    class Meta:
        verbose_name = 'Einweisung'
        verbose_name_plural = 'Einweisungen'

    def __str__(self):
        return self.title
