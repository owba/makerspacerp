from django.db import models

from makerspacerp.base.models import Introduction


class IntroductionContent(models.Model):
    introduction = models.ForeignKey(Introduction, verbose_name='Einweisung', on_delete=models.CASCADE)
    order = models.IntegerField(verbose_name='Reihenfolge')
    text = models.TextField(verbose_name='Text')

    class Meta:
        verbose_name = 'Einweisungsinhalt'
        verbose_name_plural = 'Einweisungsinhalte'
