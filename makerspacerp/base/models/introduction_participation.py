from django.db import models


class IntroductionParticipation(models.Model):
    user = models.ForeignKey('WerkstattUser', verbose_name='Teilnehmende:r', related_name='participations',
                             on_delete=models.CASCADE)
    introduction = models.ForeignKey('Introduction', verbose_name='Einweisung', on_delete=models.CASCADE)
    instructor = models.ForeignKey('WerkstattUser', verbose_name='Einweisende:r', related_name='instructed',
                                   on_delete=models.SET_NULL, null=True, blank=True)
    date = models.DateField(verbose_name='Datum')

    class Meta:
        verbose_name = 'Teilnahme'
        verbose_name_plural = 'Teilnahmen'

    def __str__(self):
        return 'Teilnahme von {} an Einweisung "{}" am {}'.format(self.user, self.introduction, self.date)
