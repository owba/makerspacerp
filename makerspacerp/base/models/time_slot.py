import datetime
from django.db import models


class TimeSlot(models.Model):
    class Types(models.TextChoices):
        RESERVATION = 'reservation', 'Reservierung'
        MACHINE = 'machine', 'Maschinenzeit'
        WORKSHOP_BASIC = 'workshop-basic', 'Werkstattzeit - BASIC'
        WORKSHOP_PLUS = 'workshop-plus', 'Werkstattzeit - PLUS'

    type = models.CharField(verbose_name='Art', max_length=200, choices=Types.choices)
    user = models.ForeignKey('WerkstattUser', verbose_name='Nutzer:in', on_delete=models.CASCADE)
    begin_time = models.DateTimeField(verbose_name='Beginn')
    end_time = models.DateTimeField(verbose_name='Ende', blank=True, null=True)
    comment = models.TextField(verbose_name='Kommentar', blank=True)

    @property
    def duration(self):
        if self.end_time:
            return round(((self.end_time - self.begin_time).seconds / 60))
        else:
            return None

    class Meta:
        verbose_name = 'Buchung'
        verbose_name_plural = 'Buchungen'

    def __str__(self):
        if self.end_time:
            end = self.end_time.strftime('%H:%M')
        else:
            end = ''

        return "Buchung von {} am {}: {}-{} Uhr".format(self.user, self.begin_time.strftime('%d.%m.%Y'),
                                                    self.begin_time.strftime('%H:%M'), end)
