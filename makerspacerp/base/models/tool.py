from django.db import models
from django.core.files import File
import tempfile
import os
import barcode


class Tool(models.Model):
    class Meta:
        verbose_name = 'Werkzeug'
        verbose_name_plural = 'Werkzeuge'

    class Types(models.TextChoices):
        SIMPLE = 'simple', 'Handwerkzeug'
        ELECTRIC = 'electric', 'Elektrowerkzeug - Stecker'
        BATTERY = 'battery', 'Elektrowerkzeug - Akku'
        MACHINE = 'machine', 'Maschine'

    image = models.ImageField(verbose_name='Bild', upload_to='tools/images', blank=True, null=True)
    barcode_str = models.CharField(verbose_name='Barcode Text', max_length=200, unique=True, editable=False)
    barcode_img = models.FileField(verbose_name='Barcode', upload_to='tools/barcodes', blank=True, null=True)
    type = models.CharField(verbose_name='Art', max_length=200, choices=Types.choices, default=Types.SIMPLE)
    name_short = models.CharField(verbose_name='Kurzbezeichnung', max_length=200)
    name_exact = models.CharField(verbose_name='exakte Bezeichnung', max_length=200, blank=True)
    manufacturer = models.CharField(verbose_name='Hersteller', max_length=200, blank=True)
    categories = models.ManyToManyField('Category', verbose_name='Kategorien', blank=True)

    def __str__(self):
        if self.name_exact:
            return "{}: {}".format(self.get_type_display(), self.name_exact)
        else:
            return "{}: {}".format(self.get_type_display(), self.name_short)

    def generate_barcode(self):
        self.barcode_str = "WRKZG-{:09d}".format(self.pk)
        temp_filedescriptor, temp_filepath = tempfile.mkstemp()
        code = barcode.get('code128', self.barcode_str)

        temp_file = open(temp_filepath, "w+b")
        code.write(temp_file)

        self.barcode_img.save("{}.svg".format(self.barcode_str), File(temp_file))
        self.save()

        temp_file.close()
        os.remove(temp_filepath)
