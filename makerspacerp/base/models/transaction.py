from django.db import models

from makerspacerp.base.models import Account


class Transaction(models.Model):
    account = models.ForeignKey(Account, verbose_name='Konto', on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=10, verbose_name='Betrag', decimal_places=2)
    time_slot = models.OneToOneField('TimeSlot', verbose_name='Zugehörige Zeitbuchung',
                                     on_delete=models.CASCADE, null=True, blank=True, default=None)
    description = models.CharField(max_length=500, verbose_name='Beschreibung')
    time_stamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Transaktion'
        verbose_name_plural = 'Transaktionen'

    def __str__(self):
        return "Buchung über {} von {}".format(self.amount, self.account)
