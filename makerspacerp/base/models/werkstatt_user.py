from datetime import datetime
from django.db import models
from django.urls import reverse
from makerspacerp.base.models import Introduction, TimeSlot
from django.db.models import Q


class WerkstattUser(models.Model):
    uuid = models.CharField(verbose_name='Karten-ID', max_length=200, blank=True)
    first_name = models.CharField(verbose_name='Vorname', max_length=200)
    last_name = models.CharField(verbose_name='Nachname', max_length=200)
    email = models.EmailField(verbose_name='Email', blank=True, null=True)
    phone = models.CharField(verbose_name='Telefon', max_length=200, blank=True, null=True)
    birth_date = models.DateField(verbose_name='Geburtsdatum')
    zip_code = models.CharField(verbose_name='PLZ', max_length=10)
    city = models.CharField(verbose_name='Stadt', max_length=200)
    street = models.CharField(verbose_name='Straße', max_length=200)
    house_nr = models.CharField(verbose_name='Hausnummer', max_length=10)
    introductions = models.ManyToManyField('Introduction', verbose_name='Teilgenommene Einweisungen',
                                           through='IntroductionParticipation', through_fields=('user', 'introduction'))
    identified = models.BooleanField(verbose_name='Ausweis vorgezeigt', default=False)
    agreed_to_terms_of_service = models.BooleanField(verbose_name='Nutzungsbedingungen zugestimmt', default=False)
    agreed_to_code_of_conduct = models.BooleanField(verbose_name='Hausordnung zugestimmt', default=False)
    subscription = models.BooleanField(verbose_name='Abo', default=False)

    class Meta:
        verbose_name = "Werkstattnutzer:in"
        verbose_name_plural = "Werkstattnutzer:innen"

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)

    def get_absolute_url(self):
        return reverse('werkstatt-user-detail', kwargs={'pk': self.pk})

    @property
    def display_name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    @property
    def address(self):
        return '{} {}, {} {}'.format(self.street, self.house_nr, self.zip_code, self.city)

    @property
    def missing_introductions(self):
        mandatory_introductions = Introduction.objects.filter(mandatory=True)
        return list(set(mandatory_introductions) - set(self.introductions.all()))

    @property
    def checked_in(self):
        checked_in_time_slots = TimeSlot.objects.filter(Q(user=self), Q(type=TimeSlot.Types.WORKSHOP_BASIC) |
                                                        Q(type=TimeSlot.Types.WORKSHOP_PLUS),
                                                        Q(begin_time__lte=datetime.now()), Q(end_time=None))

        if len(checked_in_time_slots) > 0:
            time_slot = checked_in_time_slots.first()
            return {'status': True,
                    'since': time_slot.begin_time}
        else:
            return {'status': False}
