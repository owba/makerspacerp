from django.dispatch import receiver
from django.db.models.signals import post_save
from makerspacerp.base.models import Transaction


@receiver(post_save, sender=Transaction)
def apply_transaction(sender, instance, created, **kwargs):
    if created:
        account = instance.account
        account.balance = account.balance + instance.amount
        account.save()
