from django.dispatch import receiver
from django.db.models.signals import pre_delete

from makerspacerp.base.models import Transaction


@receiver(pre_delete, sender=Transaction)
def correct_konto_balance_on_transaction_delete(sender, instance, **kwargs):
    delta = - instance.amount
    account = instance.account
    account.balance = account.balance + delta
    account.save()
