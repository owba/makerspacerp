from django.dispatch import receiver
from django.db.models.signals import pre_save

from makerspacerp.base.models import Transaction


@receiver(pre_save, sender=Transaction)
def correct_konto_balance_on_transaction_change(sender, instance, **kwargs):
    if instance.id:
        pre_edit = Transaction.objects.get(pk=instance.id)

        if pre_edit.amount != instance.amount:
            delta = pre_edit.amount - instance.amount
            account = instance.account
            account.balance = account.balance - delta
            account.save()
