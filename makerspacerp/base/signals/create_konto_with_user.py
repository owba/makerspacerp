from django.dispatch import receiver
from django.db.models.signals import post_save
from makerspacerp.base.models import WerkstattUser, Account


@receiver(post_save, sender=WerkstattUser)
def create_konto(sender, instance, created, **kwargs):
    if created:
        Account.objects.create(user=instance)
