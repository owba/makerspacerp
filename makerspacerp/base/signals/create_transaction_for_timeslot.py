import math
from decimal import Decimal

from django.dispatch import receiver
from django.db.models.signals import post_save

from makerspacerp.base.models import Transaction, TimeSlot, DashboardSettings


@receiver(post_save, sender=TimeSlot)
def create_transaction_for_timeslot(sender, instance, created, **kwargs):
    # Is timeslot of a type that requires payment, is the timeslot completed and does the user need to pay-per-use?
    # TODO: replace static TimeSlot.Types with own model for more flexibility
    if (instance.type in [TimeSlot.Types.WORKSHOP_BASIC, TimeSlot.Types.WORKSHOP_PLUS, TimeSlot.Types.MACHINE]) \
       and instance.end_time is not None \
       and (not instance.user.subscription or instance.type == TimeSlot.Types.MACHINE):

        settings = DashboardSettings.load()

        # get the respective payment intervals and prices for the types that require payment
        if instance.type == TimeSlot.Types.WORKSHOP_BASIC:
            interval = settings.workshop_basic_interval
            price = settings.workshop_basic_price
            description = "Werkstattzeit - Basic {}min"
        elif instance.type == TimeSlot.Types.WORKSHOP_PLUS:
            interval = settings.workshop_plus_interval
            price = settings.workshop_plus_price
            description = "Werkstattzeit - Plus {}min"
        else:
            interval = settings.machine_interval
            price = settings.machine_price
            description = "Maschinenzeit {}min"

        duration = instance.duration
        intervals_to_pay = math.ceil(duration/interval)
        amount = - Decimal(intervals_to_pay * price)

        # if there is no Transaction for the timeslot yet, create one
        if not Transaction.objects.filter(time_slot=instance).exists():
            Transaction.objects.create(account=instance.user.account, amount=amount, time_slot=instance,
                                       description=description.format(duration))
        # if there is update the existing one
        else:
            transaction = Transaction.objects.get(time_slot=instance)
            transaction.amount = amount
            transaction.description = description.format(duration)
            transaction.save()
