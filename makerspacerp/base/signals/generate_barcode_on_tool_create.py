from django.dispatch import receiver
from django.db.models.signals import post_save
from makerspacerp.base.models import Tool


@receiver(post_save, sender=Tool)
def generate_barcode(sender, instance, created, **kwargs):
    if created:
        instance.generate_barcode()
