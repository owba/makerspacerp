$('#charge-account__plus').on('click', increaseChargeAmount);
$('#charge-account__minus').on('click', decreaseChargeAmount);

function increaseChargeAmount() {
    var amountInput = $('#charge-account__amount');
    var amount = parseFloat(amountInput.val().replace('€','')) + 5.00
    amountInput.val(amount.toFixed(2).toString()+"€")
}

function decreaseChargeAmount() {
    var amountInput = $('#charge-account__amount');
    var amount = parseFloat(amountInput.val().replace('€','')) - 5.00
    amountInput.val(amount.toFixed(2).toString()+"€")
}