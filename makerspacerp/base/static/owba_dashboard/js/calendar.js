$(".event").each(
    function () {
        var eventBlock = $(this);

        var begin = eventBlock.data('begin').split(":");
        var beginHour = begin[0];
        var beginMins = begin[1];
        var beginMins = parseInt(beginHour)*60 + parseInt(beginMins);
        eventBlock.css('grid-row-start', ''+beginMins+'');

        if(eventBlock.data('end')==''){
            var current = new Date();
            endMins = current.getHours()*60 + current.getMinutes();
            eventBlock.addClass('running');
        }
        else {
            var end = eventBlock.data('end').split(":");
            var endHour = end[0];
            var endMins = end[1]
            var endMins = parseInt(endHour) * 60 + parseInt(endMins)
        }


        eventBlock.css('grid-row-end', ''+endMins+'');
    }
);

$(document).ready(function () {
    current = new Date();
    currentMins = current.getHours()*60 + current.getMinutes();
    $('#time').css('grid-row', ''+currentMins+'/ span 1');
});

$('.event-space').on('click', function () {
   var createViewURL = $('.cal').data('create-view');
   var beginDate = $('.cal').data('date');
   var beginTime = $(this).data('time');
   createViewURL = createViewURL + "?begin=" + beginDate + ";" + beginTime;

   window.location = createViewURL;
});