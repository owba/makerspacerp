$(document).ready(function () {
    var timer = $('#workshop-checkin-widget__timer');
    var since = timer.data('since');
    setTime(timer,since);
    setInterval(function(){setTime(timer, since);}, 1000);
});

function setTime(timer, since){
    var now = new Date()

    since = new Date(now - new Date(since));
    var sinceOffset = since.getTimezoneOffset()
    since = new Date(since.getTime() + (sinceOffset*60*1000))

    timer.text(`${since.getHours()}h ${since.getMinutes()}min ${since.getSeconds()}s`);
}
