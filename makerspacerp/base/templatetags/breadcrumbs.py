from django import template
from django.urls import reverse

register = template.Library()


@register.inclusion_tag('owba_dashboard/templatetags/breadcrumbs.html', takes_context=True)
def breadcrumbs(context):
    breadcrumb_dict = {}

    if context.get('breadcrumbs'):
        for key, url in context['breadcrumbs'].items():
            if type(url) is str:
                breadcrumb_dict[key] = reverse(url)
            elif type(url) is list:
                breadcrumb_dict[key] = reverse(url[0], kwargs={'pk': url[1]})

            context['breadcrumbs'] = breadcrumb_dict

    return context
