from django import template
import datetime

register = template.Library()


@register.filter(name='date_delta')
def date_delta(date, delta):
    if type(date) == str:
        date = datetime.datetime.strptime(date, "%Y-%m-%d")
    date = date + datetime.timedelta(days=delta)
    return date
