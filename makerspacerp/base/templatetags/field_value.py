from datetime import datetime
from django import template
from django.db.models import ManyToManyField
from django.db.models.fields.files import ImageFieldFile, FieldFile
from django.core.exceptions import FieldDoesNotExist
register = template.Library()

import logging

@register.filter(name='field_value')
def field_value(obj, field):
    try:
        value = getattr(obj, field)
    except AttributeError:
        return None

    #print(type(obj)._meta.get_field(field))

    # if Field type is a FileField and there has been a file uploaded return the file's url as field value
    if (type(value) is ImageFieldFile or type(value) is FieldFile) and value:
        value = value.url
    elif type(value) is bool:
        if value is True:
            value = 'Ja'
        else:
            value = 'Nein'
    elif value is None:
        value = '-'
    elif value is ManyToManyField:
        value = type(obj)._meta.get_field(field).value_from_object(obj)
        print('nanananan batman')

    try:
        # if the Field has a choices-attribute, return the display value instead of choice-key
        if type(obj)._meta.get_field(field).choices:
            value = dict(type(obj)._meta.get_field(field).choices)[value]
    except (FieldDoesNotExist, AttributeError):
        pass

    return value
