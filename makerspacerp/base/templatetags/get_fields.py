from django import template
from django.db.models.fields.files import ImageFieldFile, FieldFile
register = template.Library()


@register.filter(name='get_fields')
def get_fields(obj, fields):
    obj_class = type(obj)
    if fields == '__all__':
        fields = obj_class._meta.get_fields()
    else:
        flds = []
        for field in fields:
            if type(field) is dict:
                fld = obj_class._meta.get_field(field['name']).__dict__
                fld['format'] = field['format']
                flds.append(fld)
            else:
                flds.append(obj_class._meta.get_field(field))
        fields = flds
    return fields
