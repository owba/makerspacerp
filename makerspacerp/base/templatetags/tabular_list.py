from django import template
from django.core.exceptions import FieldDoesNotExist
from django.db.models.fields.reverse_related import ManyToOneRel, OneToOneRel
register = template.Library()
import logging


def reorder_columns(cols):
    """
    Takes a list of columns to be displayed via tabular_list-templatetag, determines whether any of them specifies a
    'order'-property in its render-options, reorders the list accordingly and returns it.

    :param cols: a list of dictionaries that specifies which columns shall be rendered by tabular_list-templatetag
    :return: the same list but reordered according to the 'order'-property specified in each column's render-options
    """
    def sort_helper(el):
        return el['render_options']['order']

    no_order_specified = []
    order_specified = []

    for col in cols:
        if 'render_options' in col:
            if 'order' in col['render_options']:
                order_specified.append(col)
            else:
                no_order_specified.append(col)
        else:
            no_order_specified.append(col)

    order_specified.sort(key=sort_helper)

    for col in order_specified:
        no_order_specified.insert(col['render_options']['order']-1, col)

    cols = no_order_specified

    return cols


@register.inclusion_tag('owba_dashboard/templatetags/tabular_list.html', takes_context=True)
def tabular_list(context, objects=None, columns='__all__', render_options={}, exclude=[], views=None):
    cntxt = {
        'views': views,
        }

    try:
        obj_class = type(objects[0])
    except IndexError:
        return cntxt
    column_fields = []
    order = []

    if columns == '__all__':
        columns = obj_class._meta.get_fields()
        column_names = []
        for field in columns:
            if type(field) not in [ManyToOneRel, OneToOneRel]:
                column_names.append(field.name)

        properties = [p for p in dir(obj_class) if isinstance(getattr(obj_class, p), property)]
        column_names.extend(properties)

        for exclude_field in exclude:
            if exclude_field in column_names:
                column_names.remove(exclude_field)

        columns = column_names

    for index, field in enumerate(columns):
        print("{}: {}".format(type(field), field))
        if type(field) is str:
            try:
                field = obj_class._meta.get_field(field)
                field_dict = {'name': field.name,
                              'verbose_name': field.verbose_name}
            except FieldDoesNotExist:
                print("field does not exist")
                field_dict = {
                    'name': field,
                    'verbose_name': field
                }

            # check if there exist render options for current field
            if field_dict['name'] in render_options:
                field_dict['render_options'] = render_options[field_dict['name']]

                if 'verbose_name' in field_dict['render_options']:
                    field_dict['verbose_name'] = field_dict['render_options']['verbose_name']

            column_fields.append(field_dict)

    columns = column_fields
    columns = reorder_columns(columns)

    cntxt = {
        'views': views,
        'objects': objects,
        'columns': columns,
        'request': context['request']
        }

    return cntxt
