from django import template

register = template.Library()


@register.filter(name='var_format')
def var_format(obj, format_str):
    return format_str.format(obj)
