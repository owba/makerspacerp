from django import template

register = template.Library()


@register.filter(name='verbose_name')
def verbose_name(obj):
    return obj._meta.verbose_name
