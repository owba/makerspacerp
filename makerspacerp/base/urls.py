from makerspacerp.base.views import WerkstattUserListView, WerkstattUserDetailView, WerkstattUserCreateView, \
    WerkstattUserDeleteView, WerkstattUserUpdateView, TransactionCreateView, TransactionDetailView, TransactionListView, \
    TransactionDeleteView, TransactionUpdateView, ToolDetailView, ToolCreateView, ToolDeleteView, ToolListView, \
    ToolUpdateView, IntroductionListView, IntroductionCreateView, IntroductionDetailView, IntroductionDeleteView, \
    IntroductionUpdateView, IntroductionPDFView, IntroductionParticipationCreateView, \
    IntroductionParticipationDetailView, IntroductionParticipationDeleteView, TimeSlotUpdateView, TimeSlotListView, \
    TimeSlotDetailView, TimeSlotCreateView, TimeSlotDeleteView, ToggleCheckoutView, DashboardSettingsDetailView, \
    DashboardSettingsUpdateView, ConsumableListView, ConsumableCreateView, ConsumableDetailView, GlobalSearchResults, \
    UserListView, UserDetailView, UserUpdateView, UserCreateView, UserDeleteView

from django.contrib.auth.views import LoginView, LogoutView
from django.views.generic import RedirectView

from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

urlpatterns = [
    path('', RedirectView.as_view(url='werkstattusers/'), name='index'),
    path('login/', LoginView.as_view(template_name='owba_dashboard/login.html'), name='login'),
    path('logout/', LogoutView.as_view(next_page='login'), name='logout'),
    path('search/', GlobalSearchResults.as_view(), name='global-search'),
    path('users/', UserListView.as_view(), name='user-list'),
    path('user/<int:pk>/', UserDetailView.as_view(), name='user-detail'),
    path('user/create/', UserCreateView.as_view(), name='user-create'),
    path('user/delete/<int:pk>', UserDeleteView.as_view(), name='user-delete'),
    path('user/update/<int:pk>/', UserUpdateView.as_view(), name='user-update'),
    path('werkstattusers/', WerkstattUserListView.as_view(), name='werkstatt-user-list'),
    path('werkstattuser/<int:pk>/', WerkstattUserDetailView.as_view(), name='werkstatt-user-detail'),
    path('werkstattuser/new/', WerkstattUserCreateView.as_view(), name='werkstatt-user-create'),
    path('werkstattuser/update/<int:pk>/', WerkstattUserUpdateView.as_view(), name='werkstatt-user-update'),
    path('werkstattuser/delete/<int:pk>/', WerkstattUserDeleteView.as_view(), name='werkstatt-user-delete'),
    path('werkstattuser/<int:user_id>/toggle-checkin/', ToggleCheckoutView.as_view(), name='werkstatt-user-checkin'),
    path('transaction/new/', TransactionCreateView.as_view(), name='transaction-create'),
    path('transaction/<int:pk>/', TransactionDetailView.as_view(), name='transaction-detail'),
    path('transaction/update/<int:pk>/', TransactionUpdateView.as_view(), name='transaction-update'),
    path('transaction/delete/<int:pk>/', TransactionDeleteView.as_view(), name='transaction-delete'),
    path('transactions/', TransactionListView.as_view(), name='transaction-list'),
    path('tool/new/', ToolCreateView.as_view(), name='tool-create'),
    path('tool/delete/<int:pk>/', ToolDeleteView.as_view(), name='tool-delete'),
    path('tool/<int:pk>/', ToolDetailView.as_view(), name='tool-detail'),
    path('tool/update/<int:pk>/', ToolUpdateView.as_view(), name='tool-update'),
    path('tools/', ToolListView.as_view(), name='tool-list'),
    path('introductions/', IntroductionListView.as_view(), name='introduction-list'),
    path('introduction/new/', IntroductionCreateView.as_view(), name='introduction-create'),
    path('introduction/<int:pk>/', IntroductionDetailView.as_view(), name='introduction-detail'),
    path('introduction/delete/<int:pk>/', IntroductionDeleteView.as_view(), name='introduction-delete'),
    path('introduction/update/<int:pk>/', IntroductionUpdateView.as_view(), name='introduction-update'),
    path('introduction/<int:pk>/pdf', IntroductionPDFView.as_view(), name='introduction-pdf'),
    path('introduction-participation/create/', IntroductionParticipationCreateView.as_view(), name='introduction-participation-create'),
    path('introduction-participation/<int:pk>/', IntroductionParticipationDetailView.as_view(), name='introduction-participation-detail'),
    path('introduction-participation/<int:pk>/delete', IntroductionParticipationDeleteView.as_view(), name='introduction-participation-delete'),
    path('timeslots/', TimeSlotListView.as_view(), name='time-slot-list'),
    path('timeslot/<int:pk>/', TimeSlotDetailView.as_view(), name='time-slot-detail'),
    path('timeslot/new/', TimeSlotCreateView.as_view(), name='time-slot-create'),
    path('timeslot/update/<int:pk>/', TimeSlotUpdateView.as_view(), name='time-slot-update'),
    path('timeslot/delete/<int:pk>/', TimeSlotDeleteView.as_view(), name='time-slot-delete'),
    path('settings/', DashboardSettingsDetailView.as_view(), name='settings-detail'),
    path('settings/update/', DashboardSettingsUpdateView.as_view(), name='settings-update'),
    path('storage/', ConsumableListView.as_view(), name='storage-list'),
    path('consumable/create/', ConsumableCreateView.as_view(), name='consumable-create'),
    path('consumable/<int:pk>/', ConsumableDetailView.as_view(), name='consumable-detail'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

