from makerspacerp.base.views.generic import OWBACreateView
from makerspacerp.base.models import Consumable


class ConsumableCreateView(OWBACreateView):
    model = Consumable
    icon = 'box'
    breadcrumbs = {
        'Home': 'werkstatt-user-list',
        'Verbrauchsmaterial': 'storage-list',
        'Verbrauchsmaterial anlegen': 'consumable-create'
    }
