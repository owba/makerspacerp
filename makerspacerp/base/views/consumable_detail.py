from makerspacerp.base.views.generic import OWBADetailView
from makerspacerp.base.models import Consumable


class ConsumableDetailView(OWBADetailView):
    model = Consumable
    icon = 'box'
    breadcrumbs = {
        'Home': 'werkstatt-user-list',
        'Verbrauchsmaterial': 'storage-list'
    }
