from makerspacerp.base.views.generic import OWBAListView
from makerspacerp.base.models import Consumable


class ConsumableListView(OWBAListView):
    model = Consumable
    icon = 'box'
    breadcrumbs = {
        'Home': 'werkstatt-user-list',
        'Verbrauchsmaterial': 'storage-list'
    }
    views = {
        'create': 'consumable-create',
        'detail': 'consumable-detail'
    }
    tabular_list_columns = ['name_short', 'name_exact', 'amount', 'unit', 'categories']
