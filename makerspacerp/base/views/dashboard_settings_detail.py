from makerspacerp.base.views.generic import OWBADetailView
from makerspacerp.base.models import DashboardSettings


class DashboardSettingsDetailView(OWBADetailView):
    model = DashboardSettings
    template_name = 'owba_dashboard/settings_detail.html'
    icon = 'sliders-h'
    breadcrumbs = {
        'Home': 'werkstatt-user-list',
        'Einstellungen': 'settings-detail'
    }
    update_view = 'settings-update'
    fields = [{'name': 'workshop_basic_interval', 'format': '{} min'},
              {'name': 'workshop_basic_price', 'format': '{}€'},
              {'name': 'workshop_plus_interval', 'format': '{} min'},
              {'name': 'workshop_plus_price', 'format': '{}€'},
              {'name': 'machine_interval', 'format': '{} min'}, {'name': 'machine_price', 'format': '{}€'},
              'max_people', {'name': 'max_reservation_time', 'format': '{} min'}]

    def get_object(self, queryset=None):
        obj = DashboardSettings.load()
        return obj
