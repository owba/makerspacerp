from django.urls import reverse_lazy
from makerspacerp.base.views.generic import OWBAUpdateView
from makerspacerp.base.models import DashboardSettings


class DashboardSettingsUpdateView(OWBAUpdateView):
    model = DashboardSettings
    icon = 'sliders-h'
    success_url = reverse_lazy('settings-detail')

    def get_object(self, queryset=None):
        obj = DashboardSettings.load()
        return obj
