from makerspacerp.base.views.generic.create import OWBACreateView
from makerspacerp.base.views.generic.list import OWBAListView
from makerspacerp.base.views.generic.detail import OWBADetailView
from makerspacerp.base.views.generic.delete import OWBADeleteView
from makerspacerp.base.views.generic.update import OWBAUpdateView
