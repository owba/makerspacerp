from django.views.generic import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin


class OWBACreateView(LoginRequiredMixin, CreateView):
    fields = '__all__'
    template_name = 'owba_dashboard/generic/create.html'
    icon = 'database'
    breadcrumbs = {}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['verbose_name'] = self.model._meta.verbose_name
        context['submit_label'] = "{} anlegen".format(self.model._meta.verbose_name)
        context['icon'] = self.icon
        context['breadcrumbs'] = self.breadcrumbs
        context['title'] = "{} anlegen".format(self.model._meta.verbose_name)
        return context
