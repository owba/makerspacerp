from django.views.generic.edit import DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin


class OWBADeleteView(LoginRequiredMixin, DeleteView):
    template_name = 'owba_dashboard/generic/delete.html'
    breadcrumbs = {}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['breadcrumbs'] = self.breadcrumbs

        return context
