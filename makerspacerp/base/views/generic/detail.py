from django.views.generic import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin


class OWBADetailView(LoginRequiredMixin, DetailView):
    template_name = 'owba_dashboard/generic/detail.html'
    icon = 'database'
    detail_view = None
    update_view = None
    delete_view = None
    toolbar_buttons = {}
    fields = '__all__'
    breadcrumbs = {}
    render_options = {}
    views = {}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        obj = context['object']
        context['icon'] = self.icon
        context['update_view'] = self.update_view
        context['delete_view'] = self.delete_view
        context['toolbar_buttons'] = self.toolbar_buttons
        context['breadcrumbs'] = self.breadcrumbs
        context['fields'] = self.fields
        context['render_options'] = self.render_options
        context['title'] = obj.__str__()

        if obj.__str__() not in context['breadcrumbs'] and self.detail_view:
            print(self.request.path_info)
            context['breadcrumbs'][obj.__str__()] = [self.detail_view, obj.pk]

        return context
