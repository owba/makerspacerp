from django.views.generic import ListView
from django.contrib.auth.mixins import LoginRequiredMixin


class OWBAListView(LoginRequiredMixin, ListView):
    login_url = '/login/'
    template_name = 'owba_dashboard/generic/list.html'
    icon = 'database'
    breadcrumbs = {}
    views = {}
    tabular_list_columns = '__all__'
    render_options = {}

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['verbose_name'] = self.model._meta.verbose_name
        context['title'] = self.model._meta.verbose_name_plural
        context['icon'] = self.icon
        context['breadcrumbs'] = self.breadcrumbs
        context['views'] = self.views
        context['render_options'] = self.render_options
        context['tabular_list_columns'] = self.tabular_list_columns

        return context
