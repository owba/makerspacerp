from django.views.generic.edit import UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin


class OWBAUpdateView(LoginRequiredMixin, UpdateView):
    fields = '__all__'
    template_name = 'owba_dashboard/generic/update.html'
    icon = 'database'
    breadcrumbs = {}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        obj = context['object']

        context['icon'] = self.icon
        context['breadcrumbs'] = self.breadcrumbs
        context['title'] = "'{}' bearbeiten".format(obj.__str__())

        return context
