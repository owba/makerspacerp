from django.views.generic import TemplateView
from django.shortcuts import render
from makerspacerp.base.models import WerkstattUser, Tool, TimeSlot, Consumable
from django.db.models import Value as V
from django.db.models import Q
from django.db.models.functions import Concat


class GlobalSearchResults(TemplateView):
    template_name = 'owba_dashboard/global_search_results.html'

    def get_context_data(self, **kwargs):
        search_text = self.request.GET.get('query')
        search_models = [{'object': WerkstattUser,
                          'title': 'Werkstattnutzer:innen',
                          'icon': 'users',
                          'annotate': {'full_name': Concat('first_name', V(' '), 'last_name')},
                          'filter': Q(full_name__icontains=search_text) | Q(uuid=search_text),
                          'exclude': ['zip_code', 'id', 'missing_introductions', 'pk', 'first_name', 'last_name', 'street',
                                      'house_nr', 'city', 'introductions', 'checked_in'],
                          'views': {'detail': 'werkstatt-user-detail'},
                          'render_options':  {
                                'uuid': {
                                    'verbose_name': 'ID',
                                    'width': 2
                                },
                                'display_name': {
                                    'verbose_name': 'Nutzer:in',
                                    'order': 2
                                },
                                'address': {
                                    'verbose_name': 'Adresse',
                                    'width': 4,
                                }
                            }
                          },
                         {'object': Tool,
                          'title': 'Werkzeuge/Maschinen',
                          'filter': Q(name_short__icontains=search_text) | Q(name_exact__icontains=search_text),
                          'exclude': ['barcode_img', 'barcode_str', 'pk', 'id', 'categories'],
                          'views': {'detail': 'tool-detail'},
                          'render_options': {
                                'type': {
                                    'order': 4
                                }
                            }
                          },
                         {'object': Consumable,
                          'icon': 'box',
                          'title': 'Verbrauchsmaterial',
                          'filter': Q(name_short__icontains=search_text) | Q(name_exact__icontains=search_text),
                          'exclude': ['pk', 'id', 'thumbnail', 'dimensions_x', 'dimensions_y', 'categories'],
                          'views': {'detail': 'consumable-detail'}
                          },
                         {'object': TimeSlot,
                          'icon': 'clock',
                          'title': 'Zeitbuchungen',
                          'filter': Q(comment__icontains=search_text),
                          'exclude': ['pk', 'id'],
                          'views': {'detail': 'time-slot-detail'}
                          }]

        search_results = []

        for model in search_models:
            if 'annotate' in model:
                model_results = model['object'].objects.annotate(**model['annotate']).filter(model['filter'])
            else:
                model_results = model['object'].objects.filter(model['filter'])

            if len(model_results) > 0:
                result = {}
                if 'exclude' in model:
                    result['exclude'] = model['exclude']
                if 'views' in model:
                    result['views'] = model['views']
                if 'render_options' in model:
                    result['render_options'] = model['render_options']
                if 'title' in model:
                    result['title'] = model['title']
                if 'icon' in model:
                    result['icon'] = model['icon']

                result['result'] = model_results
                search_results.append(result)

        context = {
            'title': 'Suchergebnisse für \'{}\''.format(search_text),
            'icon': 'search',
            'breadcrumbs': {
                'Home': 'werkstatt-user-list',
                'Suchergebnisse': 'global-search'
            },
            'search_results': search_results
        }

        return context
