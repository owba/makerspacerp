from django.urls import reverse_lazy

from makerspacerp.base.views.generic import OWBACreateView
from makerspacerp.base.models import Introduction


class IntroductionCreateView(OWBACreateView):
    model = Introduction
    icon = 'lightbulb'
    success_url = reverse_lazy('introduction-list')
