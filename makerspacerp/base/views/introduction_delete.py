from makerspacerp.base.views.generic import OWBADeleteView
from makerspacerp.base.models import Introduction


class IntroductionDeleteView(OWBADeleteView):
    model = Introduction
