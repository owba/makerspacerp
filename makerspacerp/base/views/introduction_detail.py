from makerspacerp.base.views.generic import OWBADetailView
from makerspacerp.base.models import Introduction


class IntroductionDetailView(OWBADetailView):
    model = Introduction
    template_name = 'owba_dashboard/introduction_detail.html'
    icon = 'lightbulb'
    breadcrumbs = {
        'Home': 'werkstatt-user-list',
        'Einweisungen': 'introduction-list'
    }
    update_view = 'introduction-update'
    delete_view = 'introduction-delete'
    toolbar_buttons = {
        'Teilnehmer:in hinzufügen': {
            'icon': 'user',
            'url': {'path': 'introduction-participation-create'}
        },
        'PDF ausgeben': {
            'icon': 'file-pdf',
            'url': {'path': 'introduction-pdf',
                    'arg': 'id'}
        },
    }
    fields = ['tool', 'mandatory', 'description']

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['toolbar_buttons']['Teilnehmer:in hinzufügen']['url']['kwargs'] = 'introduction={}'.format(context['object'].pk)

        return context
