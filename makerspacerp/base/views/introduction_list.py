from makerspacerp.base.views.generic import OWBAListView
from makerspacerp.base.models import Introduction


class IntroductionListView(OWBAListView):
    model = Introduction
    icon = 'lightbulb'
    views = {
        'create': 'introduction-create',
        'detail': 'introduction-detail',
        'update': 'introduction-update',
        'delete': 'introduction-delete'
    }
    breadcrumbs = {
        'Home': 'werkstatt-user-list',
        'Einweisungen': 'introduction-list'
    }
    tabular_list_columns = ['id', 'title', 'tool', 'mandatory']
    render_options = {
        'title':{
            'width': 4,
        },
        'tool':{
            'width': 3,
        },
        'mandatory': {
            'width': 3,
        }
    }
