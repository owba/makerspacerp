from django.urls import reverse

from makerspacerp.base.views.generic import OWBACreateView
from makerspacerp.base.models import IntroductionParticipation


class IntroductionParticipationCreateView(OWBACreateView):
    model = IntroductionParticipation
    icon = 'lightbulb'
    breadcrumbs = {
        'Home': 'werkstatt-user-list',
    }

    def get_initial(self, *args, **kwargs):
        initial = super(IntroductionParticipationCreateView, self).get_initial(**kwargs)
        initial['user'] = self.request.GET.get('user') or None
        initial['introduction'] = self.request.GET.get('introduction') or None

        return initial

    def get_success_url(self):
        user = self.request.POST.get('user')

        return reverse('werkstatt-user-detail', args=[user])
