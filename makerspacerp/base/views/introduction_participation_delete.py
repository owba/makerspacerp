from django.urls import reverse_lazy

from makerspacerp.base.views.generic import OWBADeleteView
from makerspacerp.base.models import IntroductionParticipation


class IntroductionParticipationDeleteView(OWBADeleteView):
    model = IntroductionParticipation

    def get_success_url(self):
        obj = self.get_object()

        print(obj.user)

        return reverse_lazy('werkstatt-user-detail', args=[obj.user.id])
