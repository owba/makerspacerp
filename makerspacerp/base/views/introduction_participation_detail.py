from makerspacerp.base.views.generic import OWBADetailView
from makerspacerp.base.models import IntroductionParticipation


class IntroductionParticipationDetailView(OWBADetailView):
    model = IntroductionParticipation
