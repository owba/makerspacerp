from django.views.generic import DetailView
from django_weasyprint.views import WeasyTemplateResponseMixin

from makerspacerp.base.models import Introduction


class IntroductionPDFView(WeasyTemplateResponseMixin, DetailView):
    model = Introduction
    template_name = 'owba_dashboard/introduction_pdf.html'
