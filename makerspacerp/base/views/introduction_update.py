from django.urls import reverse_lazy

from makerspacerp.base.views.generic import OWBAUpdateView
from makerspacerp.base.models import Introduction


class IntroductionUpdateView(OWBAUpdateView):
    model = Introduction
    icon = 'lightbulb'
    success_url = reverse_lazy('introduction-list')
