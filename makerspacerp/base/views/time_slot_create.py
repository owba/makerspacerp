import datetime
from django.urls import reverse_lazy
from makerspacerp.base.views.generic import OWBACreateView
from makerspacerp.base.models import TimeSlot
from makerspacerp.base.forms import TimeSlotForm


class TimeSlotCreateView(OWBACreateView):
    model = TimeSlot
    form_class = TimeSlotForm
    success_url = reverse_lazy('time-slot-list')
    fields = None
    icon = 'clock'
    breadcrumbs = {
        'Home': 'Index',
        'Buchungen': 'time-slot-list',
        'Buchung anlegen': 'time-slot-create',
    }

    def get_initial(self, *args, **kwargs):
        initial = super(TimeSlotCreateView, self).get_initial(**kwargs)
        if self.request.GET.get('begin') is not None:
            initial['begin_time'] = datetime.datetime.strptime(self.request.GET.get('begin'), "%Y-%m-%d;%H:%M:%S")
        initial['user'] = self.request.GET.get('user') or None
        initial['description'] = self.request.GET.get('description') or None

        return initial
