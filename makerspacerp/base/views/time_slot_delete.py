from django.urls import reverse_lazy
from makerspacerp.base.views.generic import OWBADeleteView
from makerspacerp.base.models import TimeSlot


class TimeSlotDeleteView(OWBADeleteView):
    model = TimeSlot
    success_url = reverse_lazy('time-slot-list')
