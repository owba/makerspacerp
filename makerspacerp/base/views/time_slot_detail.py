from makerspacerp.base.views.generic import OWBADetailView
from makerspacerp.base.models import TimeSlot


class TimeSlotDetailView(OWBADetailView):
    model = TimeSlot
    icon = 'clock'
    breadcrumbs = {
        'Home': 'werkstatt-user-list',
        'Buchungen': 'time-slot-list',
    }
    update_view = 'time-slot-update'
    delete_view = 'time-slot-delete'
