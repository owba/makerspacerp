import datetime

from makerspacerp.base.views.generic import OWBAListView
from makerspacerp.base.models import TimeSlot


class TimeSlotListView(OWBAListView):
    model = TimeSlot
    icon = 'clock'
    views = {
        'create': 'time-slot-create',
        'detail': 'time-slot-detail',
        'update': 'time-slot-update',
        'delete': 'time-slot-delete',
    }
    breadcrumbs = {
        'Home': 'werkstatt-user-list',
        'Buchungen': 'time-slot-list'
    }
    tabular_list_columns = ['begin_time', 'end_time', 'user', 'type']
    render_options = {
        'begin_time': {
            'width': 3
        },
        'end_time': {
            'width': 3
        },
        'user': {
            'width': 3
        },
        'type': {
            'width': 2
        }
    }

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        if not self.request.GET.get('view') is 'list':
            date = self.request.GET.get('date')

            if date:
                date = datetime.datetime.strptime(date, "%Y-%m-%d")

            else:
                date = datetime.date.today()

            context['date'] = date
            context['object_list'] = TimeSlot.objects.filter(begin_time__date=date)

        return context

    def get_template_names(self):
        if self.request.GET.get('view') == 'cal':
            return 'owba_dashboard/cal.html'
        else:
            return 'owba_dashboard/time_slot_list.html'

