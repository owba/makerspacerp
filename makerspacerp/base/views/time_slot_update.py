from django.urls import reverse_lazy

from makerspacerp.base.views.generic import OWBAUpdateView
from makerspacerp.base.models import TimeSlot
from makerspacerp.base.forms import TimeSlotForm


class TimeSlotUpdateView(OWBAUpdateView):
    model = TimeSlot
    icon = 'clock'
    form_class = TimeSlotForm
    fields = None
    success_url = reverse_lazy('time-slot-list')
