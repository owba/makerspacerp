from django.utils import timezone
from django.views import View
from django.shortcuts import redirect
from django.db.models import Q

from django.contrib.auth.mixins import LoginRequiredMixin
from makerspacerp.base.models import WerkstattUser, TimeSlot


class ToggleCheckoutView(LoginRequiredMixin, View):
    def get(self, *args, **kwargs):
        user = WerkstattUser.objects.get(id=kwargs['user_id'])
        next = self.request.GET.get('next')

        if user.checked_in['status'] is True:
            time_slot = TimeSlot.objects.filter(Q(user=user),
                                                Q(type=TimeSlot.Types.WORKSHOP_BASIC) |
                                                Q(type=TimeSlot.Types.WORKSHOP_PLUS),
                                                Q(begin_time__lte=timezone.now()), Q(end_time=None)).first()
            time_slot.end_time = timezone.now()
            time_slot.save()
        else:
            plan = self.request.GET.get('plan')
            if plan == 'basic':
                time_slot_type = TimeSlot.Types.WORKSHOP_BASIC
            else:
                time_slot_type = TimeSlot.Types.WORKSHOP_PLUS

            time_slot = TimeSlot.objects.create(user=user, type=time_slot_type, begin_time=timezone.now())
            time_slot.save()

        return redirect(next)
