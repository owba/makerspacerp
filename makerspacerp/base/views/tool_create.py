from makerspacerp.base.views.generic import OWBACreateView
from django.urls import reverse_lazy

from makerspacerp.base.models import Tool


class ToolCreateView(OWBACreateView):
    model = Tool
    icon = 'tools'
    breadcrumbs = {
            'Home': 'werkstatt-user-list',
            'Werkzeug/Maschinen': 'tool-list',
            'Neues Werkzeug erstellen': 'tool-create'
        }
    success_url = reverse_lazy('tool-list')
