from makerspacerp.base.views.generic import OWBADeleteView
from makerspacerp.base.models import Tool
from django.urls import reverse_lazy


class ToolDeleteView(OWBADeleteView):
    model = Tool
    success_url = reverse_lazy('tool-list')
