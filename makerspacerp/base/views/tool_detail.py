from makerspacerp.base.views.generic import OWBADetailView
from makerspacerp.base.models import Tool


class ToolDetailView(OWBADetailView):
    model = Tool
    icon = 'tools'
    breadcrumbs = {
        'Home': 'werkstatt-user-list',
        'Werkzeug/Maschinen': 'tool-list',
    }
    detail_view = 'tool-detail'
    update_view = 'tool-update'
    delete_view = 'tool-delete'
    toolbar_buttons = {
        'Label drucken': {
            'icon': 'print',
            'url': 'werkstatt-user-list'
        }
    }
    fields = ['image', 'barcode_str', 'barcode_img', 'type', 'name_short', 'name_exact', 'manufacturer']
