from makerspacerp.base.views.generic import OWBAListView
from makerspacerp.base.models import Tool


class ToolListView(OWBAListView):
    model = Tool
    icon = 'tools'
    views = {
        'create': 'tool-create',
        'detail': 'tool-detail',
        'update': 'tool-update',
        'delete': 'tool-delete'
    }
    breadcrumbs = {
        'Home': 'werkstatt-user-list',
        'Werkzeug/Maschinen': 'tool-list'
    }
    tabular_list_columns = ['barcode_str', 'barcode_img', 'image', 'name_short', 'type']
    render_options = {
        'barcode_str': {
            'verbose_name': 'ID',
            'width': 3
        },
        'barcode_img': {
            'width': 2,
            'tag': 'img'
        },
        'name_short': {
            'width': 2
        },
        'type': {
            'width': 2
        }
    }
