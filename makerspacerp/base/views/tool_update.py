from makerspacerp.base.views.generic import OWBAUpdateView
from makerspacerp.base.models import Tool

from django.urls import reverse_lazy


class ToolUpdateView(OWBAUpdateView):
    model = Tool
    icon = 'tools'
    breadcrumbs = {
        'Home': 'werkstatt-user-list',
        'Werkzeug/Maschinen': 'tool-list'
    }
    success_url = reverse_lazy('tool-list')
