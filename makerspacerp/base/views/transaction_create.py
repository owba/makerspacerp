from django.views.generic import CreateView
from django.urls import reverse

from makerspacerp.base.models import Transaction, WerkstattUser


class TransactionCreateView(CreateView):
    model = Transaction
    fields = ['account', 'amount', 'description']
    template_name = 'owba_dashboard/generic/create.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['icon'] = 'euro-sign'
        context['submit_label'] = 'Betrag buchen'
        context['breadcrumbs'] = {
            'Home': 'werkstatt-user-list',
            'Transaktionen': 'transaction-list',
            'Neue Transaktion': 'transaction-create'
        }

        return context

    def get_initial(self, *args, **kwargs):
        initial = super(TransactionCreateView, self).get_initial(**kwargs)
        initial['account'] = self.request.GET.get('account') or None
        amount = self.request.GET.get('amount')
        if amount:
            initial['amount'] = amount.replace('€', '')
        else:
            initial['amount'] = 0.00
        initial['description'] = self.request.GET.get('description') or None
        return initial

    def get_success_url(self):
        account_owner = self.object.account.user.pk
        return reverse('werkstatt-user-detail', kwargs={'pk': account_owner})
