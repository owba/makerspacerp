from django.urls import reverse_lazy
from django.views.generic.edit import DeleteView
from makerspacerp.base.models import Transaction


class TransactionDeleteView(DeleteView):
    model = Transaction
    template_name = 'owba_dashboard/generic/delete.html'
    success_url = reverse_lazy('transaction-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        obj = context['object']

        context['breadcrumbs'] = {
            'Home': 'werkstatt-user-list',
            'Transaktionen': 'transaction-list',
            '{} löschen'.format(obj): ['transaction-delete', obj.pk]
        }

        return context
