from makerspacerp.base.views.generic import OWBADetailView

from makerspacerp.base.models import Transaction


class TransactionDetailView(OWBADetailView):
    model = Transaction
    delete_view = 'transaction-delete'
    update_view = 'transaction-update'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        obj = context['object']

        context['breadcrumbs'] = {
            'Home': 'werkstatt-user-list',
            'Transaktionen': 'transaction-list',
            obj.__str__ : ['transaction-detail', obj.pk]
        }

        return context
