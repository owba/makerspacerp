from makerspacerp.base.views.generic import OWBAListView

from makerspacerp.base.models import Transaction


class TransactionListView(OWBAListView):
    model = Transaction
    template_name = 'owba_dashboard/transaction_list.html'
    icon = 'euro-sign'
    ordering = '-time_stamp'
    paginate_by = 50

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['breadcrumbs'] = {
            'Home': 'werkstatt-user-list',
            'Transaktionen': 'transaction-list'
        }

        return context
