from django.views.generic.edit import UpdateView
from django.urls import reverse_lazy

from makerspacerp.base.models import Transaction


class TransactionUpdateView(UpdateView):
    model = Transaction
    fields = '__all__'
    template_name = 'owba_dashboard/generic/update.html'
    success_url = reverse_lazy('transaction-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        obj = context['object']

        context['icon'] = 'euro-sign'
        context['breadcrumbs'] = {
            'Home': 'werkstatt-user-list',
            'Transaktionen': 'transaction-list',
            '\'{}\' bearbeiten'.format(obj): ['transaction-update', obj.pk]
        }

        return context
