from makerspacerp.base.views.generic import OWBACreateView

from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import redirect


class UserCreateView(OWBACreateView):
    model = User
    icon = 'user-cog'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = UserCreationForm

        return context

    def post(self, request, *args, **kwargs):
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password_1 = form.cleaned_data['password1']
            password_2 = form.cleaned_data['password2']

            if password_1 == password_2:
                user = User.objects.create_user(username=username, password=password_1)
                return redirect('user-list')
