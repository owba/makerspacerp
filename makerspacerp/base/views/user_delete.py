from makerspacerp.base.views.generic import OWBADeleteView

from django.contrib.auth.models import User
from django.urls import reverse_lazy


class UserDeleteView(OWBADeleteView):
    model = User
    success_url = reverse_lazy('user-list')
