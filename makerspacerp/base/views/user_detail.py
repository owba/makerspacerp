from makerspacerp.base.views.generic import OWBADetailView

from django.contrib.auth.models import User


class UserDetailView(OWBADetailView):
    model = User
    icon = 'user-cog'
    breadcrumbs = {
        'Home': 'index',
        'ERP-Nutzer:innen': 'user-list'
    }
    detail_view = 'user-detail'
    update_view = 'user-update'
    delete_view = 'user-delete'
    fields = ['username', 'email', 'first_name', 'last_name', 'last_login', 'is_superuser']


