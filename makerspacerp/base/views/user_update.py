from makerspacerp.base.views.generic import OWBAUpdateView

from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm
from django.shortcuts import redirect


class UserUpdateView(OWBAUpdateView):
    model = User
    icon = 'user-cog'
    breadcrumbs = {
        'Home': 'index',
        'ERP-Nutzer:innen': 'user-list',
    }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = UserChangeForm(instance=context['object'])
        return context

    def post(self, request, *args, **kwargs):
        form = UserChangeForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()

        return redirect('user-list')
