from makerspacerp.base.views.generic import OWBAListView

from django.contrib.auth.models import User


class UserListView(OWBAListView):
    model = User
    icon = 'user-cog'
    tabular_list_columns = ['username', 'first_name', 'last_name', 'email']
    breadcrumbs = {
        'Home': 'index',
        'Thekenkräfte': 'user-list'
    }
    views = {
        'detail': 'user-detail',
        'update': 'user-update',
        'create': 'user-create',
        'delete': 'user-delete'
    }

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Thekenkräfte'

        return context
