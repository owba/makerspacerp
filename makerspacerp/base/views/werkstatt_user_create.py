from django.views import generic
from django.urls import reverse

from makerspacerp.base.models import WerkstattUser


class WerkstattUserCreateView(generic.CreateView):
    model = WerkstattUser
    fields = ['uuid', 'first_name', 'last_name', 'birth_date', 'street', 'house_nr', 'zip_code', 'city', 'email',
              'phone', 'identified', 'agreed_to_terms_of_service', 'agreed_to_code_of_conduct', 'subscription']
    template_name = 'owba_dashboard/generic/create.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['icon'] = 'user'
        context['title'] = 'Werkstattnutzer:in erstellen'
        context['submit_label'] = 'Neue Nutzer:in erstellen'
        context['breadcrumbs'] = {
            'Home': 'werkstatt-user-list',
            'Werkstattnutzer:innen': 'werkstatt-user-list',
            'Werkstattnutzer:in erstellen': 'werkstatt-user-create'
        }
        return context

