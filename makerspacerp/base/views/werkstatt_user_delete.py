from django.urls import reverse_lazy
from django.views.generic.edit import DeleteView
from makerspacerp.base.models import WerkstattUser


class WerkstattUserDeleteView(DeleteView):
    model = WerkstattUser
    template_name = 'owba_dashboard/generic/delete.html'
    success_url = reverse_lazy('werkstatt-user-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        obj = context['object']

        context['breadcrumbs'] = {
            'Home': 'werkstatt-user-list',
            'Werkstattnutzer:innen': 'werkstatt-user-list',
            '\'{}\' löschen'.format(obj): ['werkstatt-user-delete', obj.pk]
        }

        return context
