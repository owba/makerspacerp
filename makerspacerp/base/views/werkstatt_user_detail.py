from makerspacerp.base.views.generic import OWBADetailView

from makerspacerp.base.models import WerkstattUser, Transaction, TimeSlot


class WerkstattUserDetailView(OWBADetailView):
    model = WerkstattUser
    template_name = 'owba_dashboard/werkstatt_user_detail.html'
    icon = 'user'
    update_view = 'werkstatt-user-update'
    delete_view = 'werkstatt-user-delete'
    fields = ['birth_date', 'street', 'house_nr', 'zip_code', 'city', 'subscription']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        obj = context['object']
        context['title'] = obj.__str__
        context['transaction_list'] = Transaction.objects.filter(account__user=self.object).order_by("-time_stamp")[:5]
        context['transaction_detail_view'] = 'transaction-detail'
        context['breadcrumbs'] = {
            'Home': 'werkstatt-user-list',
            'Werkstattnutzer:innen': 'werkstatt-user-list',
            obj.__str__: ['werkstatt-user-detail', obj.pk],
        }
        context['introduction_list_columns'] = ['date', 'introduction']
        context['introduction_participation_views'] = {
            'create': 'introduction-participation-create',
            'delete': 'introduction-participation-delete',
            'detail': 'introduction-participation-detail',
            'create_initial': 'user={}'.format(self.object.id)
        }
        context['time_slots'] = {
            'objects': TimeSlot.objects.filter(user=self.object).order_by('-begin_time')[:5],
            'columns': ['begin_time', 'end_time', 'duration', 'type'],
            'views': {
                'create': 'time-slot-create',
                'detail': 'time-slot-detail',
                'update': 'time-slot-update',
                'delete': 'time-slot-delete'
            },
            'render_options': {
                'begin_time': {
                    'width': 3,
                    'date_format': 'd.m.y - H:i'
                },
                'end_time': {
                    'width': 3,
                    'date_format': 'd.m.y - H:i'
                },
                'duration': {
                    'verbose_name': 'Dauer',
                    'width': 2
                },
                'type': {
                    'width': 3
                }
            }
        }

        context['detail_view'] = 'introduction-participation-detail'
        context['render_options'] = {
            'date': {
                'width': 3
            },
            'introduction': {
                'width': 8
            }
        }

        return context
