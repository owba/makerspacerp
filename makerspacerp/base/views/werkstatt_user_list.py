from makerspacerp.base.views.generic import OWBAListView

from makerspacerp.base.models import WerkstattUser


class WerkstattUserListView(OWBAListView):
    model = WerkstattUser
    icon = 'users'
    template_name = 'owba_dashboard/werkstatt_user_list.html'
    paginate_by = 50
    breadcrumbs = {
        'Home': 'werkstatt-user-list',
        'Werkstattnutzer:innen': 'werkstatt-user-list',
    }
    views = {
        'create': 'werkstatt-user-create',
        'detail': 'werkstatt-user-detail',
        'update': 'werkstatt-user-update',
        'delete': 'werkstatt-user-delete'
    }
    tabular_list_columns = ['uuid', 'first_name', 'last_name', 'birth_date']
    render_options = {
        'uuid': {
          'width': 2
        },
        'first_name': {
            'width': 2
        },
        'last_name': {
            'width': 2
        },
        'birth_date': {
            'width': 2
        }
    }

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Nutzer:innenverwaltung"

        return context

    def get_checked_in_werkstatt_users(self):
        pass
