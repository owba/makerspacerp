from django.views.generic.edit import UpdateView

from makerspacerp.base.models import WerkstattUser


class WerkstattUserUpdateView(UpdateView):
    model = WerkstattUser
    fields = ['uuid', 'first_name', 'last_name', 'birth_date', 'street', 'house_nr', 'zip_code', 'city', 'email',
              'phone', 'identified', 'agreed_to_terms_of_service', 'agreed_to_code_of_conduct', 'subscription']
    template_name = 'owba_dashboard/generic/update.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        obj = context['object']

        context['icon'] = 'user'
        context['breadcrumbs'] = {
            'Home': 'werkstatt-user-list',
            'Werkstattnutzer:innen': 'werkstatt-user-list',
            ' \'{}\' bearbeiten'.format(obj): ['werkstatt-user-update', obj.pk]
        }

        return context
